import React, { useState } from 'react';
import './App.scss';
import SearchBox from './components/searchBox/searchBox';
import { gitLabApi } from './model/gitLabHttp';
import { IUser, UserMessage } from './model/userBoilerplate'
import UserProfile from './components/userProfile/userProfile';
import UserProjects from './components/userProjects/userProjects';

const App = () => {
  const [user, setUser] = useState<IUser>()
  const [systemMessage, setSystemMessage] = useState<UserMessage>(UserMessage.INIT_MESSAGE)

  const onSearchClicked = async (username?: string) => {
    setUser(undefined)
    if (username) {
      setSystemMessage(UserMessage.LOADING)
      let newUser: IUser[] = []
      try {
        newUser = await gitLabApi<IUser[]>(`https://gitlab.com/api/v4/users?username=${username}`)
        if (newUser.length > 0) {
          setUser({ ...newUser[0] })
        } else {
          setSystemMessage(UserMessage.USER_NOT_FOUND)
        }
      } catch (error) {
        setSystemMessage(UserMessage.GENERAL_ERROR)
      }
    } else {
      setSystemMessage(UserMessage.INIT_MESSAGE)
    }
  }
  return (
    <div className="App">
      <SearchBox onSearchClicked={onSearchClicked} />
      {
        user && <UserProfile user={user} />
      }
      {
        user && <UserProjects user={user} />
      }
      {
        !user && <span>{systemMessage}</span>
      }
    </div>
  );
}

export default App;

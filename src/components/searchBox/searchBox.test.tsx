import React from 'react';
import { render } from '@testing-library/react';
import SearchBox from './searchBox'

test('renders learn react link', () => {
    const searchBox = render(<SearchBox onSearchClicked={() => { }} />);
    expect(searchBox).toMatchSnapshot();
});

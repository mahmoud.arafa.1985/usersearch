import React, { useState } from 'react'
import SearchIcon from "../../img/search.svg";
import './searchBox.scss'

interface Props {
    onSearchClicked: (searchText?: string) => void
}

const SearchBox: React.FC<Props> = (props) => {
    const [searchText, setSearchText] = useState()
    return (
        <div className='search__container' >
            <div className='search__container__controls'>
                <img src={SearchIcon} alt='search icon' />
                <input type="text" placeholder="Search for GitLab users by username..."
                    defaultValue={searchText}
                    onChange={(ev) => setSearchText(ev.target.value)} />
            </div>
            <button className='search__button'
                onClick={() => props.onSearchClicked(searchText)}>Search</button>
        </div>
    )
}

export default SearchBox

import React from 'react'
import './labelControl.scss'

interface Props {
    label: string
    value?: string
}

const LabelControl: React.FC<Props> = (pros) => {
    const { label, value } = pros
    return (
        <div className='labelContainer'>
            <label className='labelText' htmlFor={label}>{label}</label>
            <label className='labelValue' htmlFor={value}>{value ? value : 'not available'}</label>
        </div>
    )
}

export default LabelControl

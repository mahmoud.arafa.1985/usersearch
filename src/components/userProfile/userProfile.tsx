import React, { useState, useEffect } from 'react'
import './userProfile.scss';
import LabelControl from '../labelControl/labelControl';
import { IUserProfile, IUser } from '../../model/userBoilerplate';
import { gitLabApi } from '../../model/gitLabHttp';

interface Props {
    user: IUser
}

const UserProfile: React.FC<Props> = (props) => {
    const { id, name, username, web_url, avatar_url } = props.user
    const [userProfileState, setUserProfileState] = useState<IUserProfile>()

    useEffect(() => {
        const getUserProfile = async () => {
            const userProfile: IUserProfile =
                await gitLabApi<IUserProfile>(`https://gitlab.com/api/v4/users/${id}`)
            if (userProfile) {
                setUserProfileState(userProfile)
            }
        }
        getUserProfile()
    }, [id])
    return (
        <div className='profile'>
            <div className="profile_header">
                <img src={avatar_url} alt={name} />
            </div>
            <div className="info">
                <ul className='userInfo'>
                    <li><LabelControl label='User ID:' value={id.toString()} /></li>
                    <li><LabelControl label='Name:' value={name} /></li>
                    <li><LabelControl label='User Name:' value={username} /></li>
                    <li><LabelControl label='User Page:' value={web_url} /></li>
                    <li><LabelControl label='Website:' value={userProfileState?.website_url} /></li>
                </ul>
                {userProfileState && <ul className='additionalInfo'>
                    <li><LabelControl label='Email:' value={userProfileState.public_email} /></li>
                    <li><LabelControl label='created at:' value={userProfileState.created_at} /></li>
                    <li><LabelControl label='Bio:' value={userProfileState.bio} /></li>
                    <li><LabelControl label='Location:' value={userProfileState.location} /></li>
                    <li><LabelControl label='Skype:' value={userProfileState.skype} /></li>
                    <li><LabelControl label='twitter:' value={userProfileState.twitter} /></li>
                    <li><LabelControl label='LinkedIn:' value={userProfileState.linkedin} /></li>
                </ul>}
                <ul></ul>
            </div>
        </div>
    )
}

export default UserProfile

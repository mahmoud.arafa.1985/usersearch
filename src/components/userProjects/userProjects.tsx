import React, { useEffect, useState } from 'react'
import { IUserProjects, IUser } from '../../model/userBoilerplate'
import { gitLabApi } from '../../model/gitLabHttp'
import LabelControl from '../labelControl/labelControl'
import './userProjects.scss'
import { formatDate } from '../../model/dateFormater'

interface Props {
    user: IUser
}
const UserProjects: React.FC<Props> = (props) => {
    const { id } = props.user
    const [userProjects, setUserProjects] = useState<IUserProjects[]>()
    useEffect(() => {
        const getUserProjects = async () => {
            const userProjects: IUserProjects[] =
                await gitLabApi<IUserProjects[]>(`https://gitlab.com/api/v4/users/${id}/projects`)
            if (userProjects) {
                setUserProjects(userProjects)
            }
        }
        getUserProjects()
    }, [id])
    return (
        <div className='projects'>
            <h3>User Projects</h3>
            <div className='projectsList'>
                {
                    userProjects && userProjects.length > 0 && userProjects.map((project) => {

                        return <ul key={project.id} className='userInfo'>
                            <li><LabelControl label='Project ID:' value={project.id?.toString()} /></li>
                            <li><LabelControl label='Project Name:' value={project.name} /></li>
                            <li><LabelControl label='Description:' value={project.description} /></li>
                            <li><LabelControl label='Created at:' value={formatDate(project.created_at)}/></li>
                            <li><LabelControl label='Visibility:' value={project.visibility} /></li>
                            <li><LabelControl label='last activity:' value={formatDate(project.last_activity_at)} /></li>
                        </ul>
                    })
                }
            </div>
        </div>
    )
}
export default UserProjects
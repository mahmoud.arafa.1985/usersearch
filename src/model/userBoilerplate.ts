export enum UserMessage {
    INIT_MESSAGE = 'Please use search box, to get users from GitLab.',
    USER_NOT_FOUND = 'User not found !',
    GENERAL_ERROR = 'General error, please check the internet connection. ',
    LOADING = 'Loading...'
}

export interface IUser {
    id: number
    name: string
    username: string
    state: string
    avatar_url: string
    web_url: string
}

export interface IUserProfile {
    created_at?: string
    bio?: string
    location?: string
    public_email?: string
    skype?: string
    linkedin?: string
    twitter?: string
    website_url?: string
    organization?: string
}

export interface IUserProjects {
    id?: number
    description?: string
    name?: string
    created_at?: string
    visibility?: string
    last_activity_at?: string

}
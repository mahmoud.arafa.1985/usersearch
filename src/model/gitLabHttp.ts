const httpBody = {
    headers: {
        'PRIVATE-TOKEN': 'v9Hu-S-QzbQqDfwgPQeL'
    }
}

export const gitLabApi = async <T>(url: string): Promise<T> => {
    const response = await fetch(url, httpBody);
    const data = await response.json();
    return data;
}